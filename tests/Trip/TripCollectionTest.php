<?php

namespace Tests\Trip;

use App\Cards\Types\Bus;
use App\Cards\Types\Flight;
use App\Cards\Types\Train;
use App\Trip\TripCollection;
use Tests\TestCase;

class TripCollectionTest extends TestCase
{
    protected $tripCollection;

    protected $cards;

    public function setUp()
    {
        parent::setUp();

        $this->cards = [];

        $this->cards[] = ( new Bus() )->hydrate([
            'from' => 'LocationA',
            'to' => 'LocationB'
        ]);

        $this->cards[] = ( new Train() )->hydrate([
            'from' => 'LocationB',
            'to' => 'LocationC'
        ]);

        $this->cards[] = ( new Flight() )->hydrate([
            'from' => 'LocationC',
            'to' => 'LocationD',
            'gate' => 'Gate'
        ]);

        $this->tripCollection = new TripCollection( $this->cards );
    }

    public function testInstanceConstruction()
    {
        $this->assertInstanceOf(TripCollection::class, $this->tripCollection);
    }

    public function testToStringSuccessful()
    {
        $expects = "\nTake the airport bus from LocationA to LocationB. No seat assignment."
                  ."\nTake train  from LocationB to LocationC. No seat assignment."
                  ."\nFrom LocationC take flight  to LocationD. Gate Gate, No seat assignment. Baggage will be automatically transferred from your last leg."
                  ."\nYou have arrived at your final destination";

        $message = $this->tripCollection->toString();

        $this->assertEquals($message, $expects);
    }

    public function testContainerSuccessful()
    {
        $this->assertEquals($this->cards, $this->tripCollection->container());
    }
}