<?php

namespace Tests\Trip;

use App\Cards\Types\Bus;
use App\Cards\Types\Flight;
use App\Cards\Types\Train;
use App\Trip\TripCollection;
use App\Trip\TripSorter;
use Tests\TestCase;

class TripSorterTest extends TestCase
{
    private $tripCollection;

    private $tripSorter;

    private $expected;

    public function setUp()
    {
        parent::setUp();

        $cards = [];

        $cards[] = ( new Bus() )->hydrate([
            'from' => 'LocationA',
            'to' => 'LocationB'
        ]);

        $cards[] = ( new Train() )->hydrate([
            'from' => 'LocationB',
            'to' => 'LocationC'
        ]);

        $cards[] = ( new Flight() )->hydrate([
            'from' => 'LocationC',
            'to' => 'LocationD',
            'gate' => 'Gate'
        ]);

        $cards[] = ( new Bus() )->hydrate([
            'from' => 'LocationD',
            'to' => 'LocationE'
        ]);

        $this->expected = $cards;

        shuffle($cards);

        $this->tripCollection = new TripCollection($cards);

        $this->tripSorter = new TripSorter( $this->tripCollection );
    }

    public function testInstanceConstruction()
    {
        $this->assertInstanceOf(TripSorter::class, $this->tripSorter);
    }

    public function testSortSuccessfull() {

        $tripCollectionSorted = $this->tripSorter->sort();

        $items = $tripCollectionSorted->container();
        $expectedCollection = $this->expected;

        for ($i = 0; $i < count($items); $i++) {

            $item = $items[$i];
            $expected = $expectedCollection[$i];

            $this->assertEquals($item->from(), $expected->from());
            $this->assertEquals($item->to(), $expected->to());
        }
    }

    public function testExtractFirstAndLastSuccessfull() {

        $tripSorter = $this->tripSorter->extractFirstAndLast();

        $first = reset($this->expected);
        $last = end($this->expected);

        $this->assertEquals($first->from(), $tripSorter->firstTripDeparture);
        $this->assertEquals($last->to(), $tripSorter->lastTripArrival);
    }

    public function testOrderSuccessfull() {

        $tripSorter = $this->tripSorter->extractFirstAndLast()
                                       ->order();

        $this->assertInstanceOf(TripSorter::class, $tripSorter);
    }
}