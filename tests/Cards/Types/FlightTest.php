<?php

namespace Tests\Cards\Types;

use App\Cards\Types\Flight;
use Tests\TestCase;

/**
 * Class FlightTest
 * @package App\Test
 */
class FlightTest extends TestCase
{
    protected $flight;

    public function setUp()
    {
        parent::setUp();

        $this->flight = new Flight();
    }

    public function testInstanceConstruction()
    {
        $obj = new Flight();
        $this->assertInstanceOf(Flight::class, $obj);
    }

    public function testGetGateSuccessful()
    {
        $value = 'Gate';
        $this->flight->gate = $value;

        $this->assertEquals($value, $this->flight->gate());
    }

    public function testGetBaggageDropCounterSuccessful()
    {
        $value = 'BaggageDropCounter';
        $this->flight->baggageDropCounter = $value;

        $this->assertEquals($value, $this->flight->baggageDropCounter());
    }

    public function testFlightMessageSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'transportCode' => 'TransportCode',
            'gate' => 'Gate'
        ];

        $this->flight->hydrate( $data );

        $message = $this->flight->flightMessage();
        $expects = 'From '. $data['from'] .' take flight '. $data['transportCode'] .' to '. $data['to'] .'. Gate '. $data['gate'];

        $this->assertEquals($message, $expects);
    }

    public function testBaggageMessageWithCounterSuccessful()
    {
        $data = [
            'baggageDropCounter' => '999',
        ];

        $this->flight->hydrate( $data );

        $message = $this->flight->baggageMessage();
        $expects = "Baggage drop at ticket counter ". $data['baggageDropCounter'] .".";

        $this->assertEquals($message, $expects);
    }

    public function testBaggageMessageWithoutCounterSuccessful()
    {
        $message = $this->flight->baggageMessage();
        $expects = "Baggage will be automatically transferred from your last leg.";

        $this->assertEquals($message, $expects);
    }

    public function testToStringWithSeatSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'transportCode' => 'TransportCode',
            'gate' => 'Gate',
            'seat' => 'Seat'
        ];

        $this->flight->hydrate( $data );

        $message = $this->flight->toString();
        $expects = 'From '. $data['from'] .' take flight '. $data['transportCode'] .' to '. $data['to'] .'. Gate '. $data['gate'];
        $expects .= ', Seat in seat '. $data['seat'] .'.';
        $expects .= ' Baggage will be automatically transferred from your last leg.';

        $this->assertEquals($message, $expects);
    }

    public function testToStringWithoutSeatSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'transportCode' => 'TransportCode',
            'gate' => 'Gate'
        ];

        $this->flight->hydrate( $data );

        $message = $this->flight->toString();
        $expects = 'From '. $data['from'] .' take flight '. $data['transportCode'] .' to '. $data['to'] .'. Gate '. $data['gate'];
        $expects .= ', No seat assignment.';
        $expects .= ' Baggage will be automatically transferred from your last leg.';

        $this->assertEquals($message, $expects);
    }
}
