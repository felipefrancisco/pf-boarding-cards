<?php

namespace Tests\Cards\Types;

use App\Cards\Types\Train;
use Tests\TestCase;

/**
 * Class TrainTest
 * @package App\Test
 */
class TrainTest extends TestCase
{
    protected $train;

    public function setUp()
    {
        parent::setUp();

        $this->train = new Train();
    }

    public function testInstanceConstruction()
    {
        $obj = new Train();
        $this->assertInstanceOf(Train::class, $obj);
    }

    public function testTrainMessageSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'transportCode' => 'TransportCode'
        ];

        $this->train->hydrate( $data );

        $message = $this->train->trainMessage();
        $expects = 'Take train '. $data['transportCode'] . ' from '. $data['from'] . ' to '. $data['to'] . '.';

        $this->assertEquals($message, $expects);
    }

    public function testToStringWithoutSeatSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'transportCode' => 'TransportCode'
        ];

        $this->train->hydrate( $data );

        $message = $this->train->toString();
        $expects = 'Take train '. $data['transportCode'] . ' from '. $data['from'] . ' to '. $data['to'] . '. No seat assignment.';

        $this->assertEquals($message, $expects);
    }

    public function testToStringWithSeatSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'transportCode' => 'TransportCode',
            'seat' => 'Seat'
        ];

        $this->train->hydrate( $data );

        $message = $this->train->toString();
        $expects = 'Take train '. $data['transportCode'] . ' from '. $data['from'] . ' to '. $data['to'] . '. Seat in seat '. $data['seat'] .'.';

        $this->assertEquals($message, $expects);
    }
}
