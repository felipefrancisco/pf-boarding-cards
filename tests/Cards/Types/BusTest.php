<?php

namespace Tests\Cards\Types;

use App\Cards\Types\Bus;
use Tests\TestCase;

/**
 * Class BusTest
 * @package App\Test
 */
class BusTest extends TestCase
{
    protected $bus;

    public function setUp()
    {
        parent::setUp();

        $this->bus = new Bus();
    }

    public function testInstanceConstruction()
    {
        $obj = new Bus();
        $this->assertInstanceOf(Bus::class, $obj);
    }

    public function testBusMessageSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To'
        ];

        $this->bus->hydrate( $data );

        $message = $this->bus->busMessage();
        $expects = 'Take the airport bus from '. $data['from'] . ' to '. $data['to'] . '.';

        $this->assertEquals($message, $expects);
    }

    public function testToStringWithoutSeatSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To'
        ];

        $this->bus->hydrate( $data );

        $message = $this->bus->toString();
        $expects = 'Take the airport bus from '. $data['from'] . ' to '. $data['to'] . '. No seat assignment.';

        $this->assertEquals($message, $expects);
    }

    public function testToStringWithSeatSuccessful()
    {
        $data = [
            'from' => 'From',
            'to' => 'To',
            'seat' => 'Seat'
        ];

        $this->bus->hydrate( $data );

        $message = $this->bus->toString();
        $expects = 'Take the airport bus from '. $data['from'] . ' to '. $data['to'] . '. Seat in seat '. $data['seat'] .'.';

        $this->assertEquals($message, $expects);
    }
}
