<?php

namespace Tests;

use App\Cards\CardFactory;
use App\Cards\Types\Bus;
use App\Cards\Types\Flight;
use App\Cards\Types\Train;

/**
 * Class CardFactoryTest
 * @package App\Test
 */
class CardFactoryTest extends TestCase
{
    /**
     * @var CardFactory
     */
    protected $cardFactory;

    public function setUp()
    {
        parent::setUp();
        $this->cardFactory = new CardFactory();
    }

    public function testInstanceConstruction()
    {
        $this->assertInstanceOf(CardFactory::class, $this->cardFactory);
    }

    public function testMakeValidBus()
    {
        $method = self::reflectMethod(CardFactory::class, 'make');

        $json = new \stdClass();
        $json->transport = 'bus';

        $card = $method->invokeArgs($this->cardFactory, [$json]);

        $this->assertInstanceOf(Bus::class, $card);
    }

    public function testMakeValidFlight()
    {

        $method = self::reflectMethod(CardFactory::class, 'make');

        $json = new \stdClass();
        $json->transport = 'flight';

        $card = $method->invokeArgs($this->cardFactory, [$json]);

        $this->assertInstanceOf(Flight::class, $card);
    }

    public function testMakeValidTrain()
    {

        $method = self::reflectMethod(CardFactory::class, 'make');

        $json = new \stdClass();
        $json->transport = 'train';

        $card = $method->invokeArgs($this->cardFactory, [$json]);

        $this->assertInstanceOf(Train::class, $card);
    }

    public function testMakeInvalidCardType()
    {
        $this->expectException(\Exception::class);

        $json = new \stdClass();
        $json->transport = 'invalid';


        $method = self::reflectMethod(CardFactory::class, 'make');

        $method->invokeArgs($this->cardFactory, [$json]);
    }

    public function testMakeInvalidCardStructure()
    {
        $this->expectException(\TypeError::class);

        $json = [];


        $method = self::reflectMethod(CardFactory::class, 'make');

        $method->invokeArgs($this->cardFactory, [$json]);
    }

    public function testResolveFailure()
    {
        $this->expectException(\Exception::class);

        $method = self::reflectMethod(CardFactory::class, 'resolve');

        $method->invokeArgs($this->cardFactory, ['invalid']);
    }


    public function testResolveSuccessful()
    {
        $method = self::reflectMethod(CardFactory::class, 'resolve');

        $result = $method->invokeArgs($this->cardFactory, ['bus']);
        $this->assertEquals(Bus::class, $result);

        $result = $method->invokeArgs($this->cardFactory, ['flight']);
        $this->assertEquals(Flight::class, $result);

        $result = $method->invokeArgs($this->cardFactory, ['train']);
        $this->assertEquals(Train::class, $result);
    }


    public function testTypesSuccessful()
    {
        $method = self::reflectMethod(CardFactory::class, 'types');

        $result = $method->invokeArgs($this->cardFactory, []);

        $this->assertEquals( [
            'bus'    => Bus::class,
            'flight' => Flight::class,
            'train'  => Train::class
        ], $result);
    }
}
