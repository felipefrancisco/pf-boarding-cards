<?php

namespace Tests\Cards;

use Tests\TestCase;
use App\Cards\Card;

/**
 * Class CardFactoryTest
 * @package App\Test
 */
class CardFactoryTest extends TestCase
{
    /**
     * @var CardFactory
     */
    protected $card;

    public function setUp()
    {parent::setUp();
        $this->card = $this->getMockForAbstractClass( Card::class );
    }

    public function testGetFromSuccessful()
    {$value = 'From';
        $this->card->from = $value;

        $this->assertEquals($value, $this->card->from());
    }

    public function testGetToSuccessful()
    {
        $value = 'To';
        $this->card->to = $value;

        $this->assertEquals($value, $this->card->to());
    }

    public function testGetSeatSuccessful()
    {
        $value = 'Seat';
        $this->card->seat = $value;

        $this->assertEquals($value, $this->card->seat());
    }

    public function testGetTransportCodeSuccessful()
    {
        $value = 'TransportCode';
        $this->card->transportCode = $value;

        $this->assertEquals($value, $this->card->transportCode());
    }

    public function testSeatMessageWithSeatSuccessful()
    {$this->card->seat = '10B';

        $message = 'Seat in seat '. $this->card->seat .'.';

        $this->assertEquals($message, $this->card->seatMessage());
    }

    public function testSeatMessageWithoutSeatSuccessful()
    {$message = 'No seat assignment.';

        $this->assertEquals($message, $this->card->seatMessage());
    }

    public function testHydrateSuccessful()
    {$data = [
            'from' => 'From',
            'to'   => 'To',
            'seat' => 'Seat',
            'invalidProperty' => true
        ];

        $obj = $this->card->hydrate($data);

        $this->assertEquals($obj->from(), $data['from']);
        $this->assertEquals($obj->to(), $data['to']);
        $this->assertEquals($obj->seat(), $data['seat']);
        $this->assertNotTrue(property_exists($obj, 'invalidProperty'));
    }
}
