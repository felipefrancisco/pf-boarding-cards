<?php

namespace Tests;

/**
 * Class TestCase
 * @package App\Test
 */
class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * Creates method reflection.
     *
     * @param string $class
     * @param string $method
     * @return ReflectionMethod
     */
    public static function reflectMethod($class, $method)
    {
        $reflection = new \ReflectionClass($class);

        $method = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }
}
