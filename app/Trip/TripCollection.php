<?php

namespace App\Trip;

use App\Cards\Card;

/**
 * Class TripCollection
 * @package App\Trip
 */
class TripCollection
{
    /**
     * @var Card[]
     */
    private $container;

    /**
     * TripCollection constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->container = $data;
    }

    public function toString() {

        $message = '';

        foreach($this->container as $item) {

            $message .= "\n". $item->toString();
        }

        $message .= "\nYou have arrived at your final destination";

        return $message;
    }

    /**
     * @return Card[]|array
     */
    public function container() {

        return $this->container;
    }
}