<?php

namespace App\Trip;

/**
 * Class TripSorter
 * @package App\Trip
 */
class TripSorter
{
    /**
     * @var TripCollection
     */
    private $tripCollection;

    public $firstTripDeparture;

    public $lastTripArrival;

    private $departures = [];

    private $arrivals = [];

    /**
     * TripSorter constructor.
     * @param TripCollection $tripCollection
     */
    public function __construct(TripCollection $tripCollection)
    {
        $this->tripCollection = $tripCollection;
    }

    /**
     * @return TripCollection
     */
    public function sort() {

        $this->extractFirstAndLast()
             ->order();

        return $this->tripCollection;
    }

    /**
     * @return $this
     */
    public function extractFirstAndLast() {

        $trips = $this->tripCollection->container();

        foreach($trips as $trip) {

            $this->departures[] = $trip->from();
            $this->arrivals[] = $trip->to();
        }

        $this->firstTripDeparture = current(array_diff($this->departures, $this->arrivals));
        $this->lastTripArrival = current(array_diff($this->arrivals, $this->departures));

        return $this;
    }

    /**
     * @return $this
     */
    public function order() {

        $trips = $this->tripCollection->container();
        $sorted = [];

        for($i = 0; $i < count($trips); $i++) {

            $trip = $trips[$i];

            if ($this->firstTripDeparture == $trip->from() || (count($sorted) >= 1 && end($sorted)->to() == $trip->from())) {

                $sorted[] = $trip;
                array_splice($trips, $i, 1);
                $i = -1;
                continue;
            }
        }

        $this->tripCollection = new TripCollection( $sorted );

        return $this;
    }
}