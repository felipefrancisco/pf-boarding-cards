<?php

namespace App\Cards\Types;

use App\Cards\Card;
use App\Cards\Contracts\Card as CardContract;

/**
 * Class Flight
 * @package App\Cards\Types
 */
class Flight extends Card implements CardContract
{
    public $gate;

    public $baggageDropCounter;

    /**
     * @return mixed
     */
    public function gate()
    {
        return $this->gate;
    }

    /**
     * @return mixed
     */
    public function baggageDropCounter()
    {
        return $this->baggageDropCounter;
    }

    /**
     * @return string
     */
    public function flightMessage() {

        return 'From '. $this->from() .' take flight '. $this->transportCode() .' to '. $this->to() .'. Gate '. $this->gate();
    }

    /**
     * @return string
     */
    public function baggageMessage() {

        if (! $this->baggageDropCounter() ) {
            return "Baggage will be automatically transferred from your last leg.";
        }

        return "Baggage drop at ticket counter ". $this->baggageDropCounter() .".";
    }

    /**
     * @return string
     */
    public function toString() {

        return $this->flightMessage() .', '. $this->seatMessage() .' '. $this->baggageMessage();
    }
}
