<?php

namespace App\Cards\Types;

use App\Cards\Card;
use App\Cards\Contracts\Card as CardContract;

/**
 * Class Bus
 * @package App\Cards\Types
 */
class Bus extends Card implements CardContract
{
    /**
     * @return string
     */
    public function busMessage()
    {
        return 'Take the airport bus from '. $this->from() . ' to '. $this->to() . '.';
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->busMessage() .' '. $this->seatMessage();

    }
}
