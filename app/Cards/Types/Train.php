<?php

namespace App\Cards\Types;

use App\Cards\Card;
use App\Cards\Contracts\Card as CardContract;

/**
 * Class Train
 * @package App\Cards\Types
 */
class Train extends Card implements CardContract
{
    /**
     * @return string
     */
    public function trainMessage() {

        return 'Take train '. $this->transportCode() .' from '. $this->from() .' to '. $this->to() .'.';
    }

    /**
     * @return string
     */
    public function toString() {

        return $this->trainMessage() .' '. $this->seatMessage();
    }
}
