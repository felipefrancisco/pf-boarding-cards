<?php

namespace App\Cards\Sourcers;

/**
 * Class Sourcer
 * @package App\Cards\Sourcers
 */
class Sourcer
{
    private $contents;

    /**
     * Sourcer constructor.
     * @param $path
     * @throws \Exception
     */
    public function __construct($path)
    {
        if(!is_file($path))
            throw new \Exception('Invalid Path.');

        $this->contents = file_get_contents($path);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return json_decode($this->contents);
    }
}