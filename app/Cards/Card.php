<?php

namespace App\Cards;

use App\Cards\Contracts\Card as CardContract;

/**
 * Class Card
 * @package App\Cards
 */
abstract class Card implements CardContract
{
    /**
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $to;

    /**
     * @var string
     */
    public $seat;

    /**
     * @var string
     */
    public $transportCode;

    /**
     * @return string
     */
    public function from()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function to()
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function seat()
    {
        return $this->seat;
    }

    /**
     * @return string
     */
    public function transportCode()
    {
        return $this->transportCode;
    }

    /**
     * @return string
     */
    public function seatMessage() {

        if( !$this->seat() ) {
            return 'No seat assignment.';
        }

        return 'Seat in seat '. $this->seat() .'.';
    }

    /**
     * @param $data
     * @return $this
     */
    public function hydrate($data)
    {
        $class = get_class($this);

        foreach ($data as $property => $value) {

            if(property_exists($class, $property)) {

                $this->{$property} = $value;
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    abstract public function toString();
}