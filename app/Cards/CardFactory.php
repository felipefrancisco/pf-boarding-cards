<?php

namespace App\Cards;

use App\Cards\Types\Bus;
use App\Cards\Types\Flight;
use App\Cards\Types\Train;

/**
 * Class CardFactory
 * @package App\Cards
 */
class CardFactory
{
    /**
     * @param $json
     * @return Card
     */
    public function make( \stdClass $json)
    {
        $class = $this->resolve( $json->transport );

        $reflection = new \ReflectionClass($class);

        /** @var Card $instance */
        $instance = $reflection->newInstance();

        return $instance->hydrate($json);
    }

    /**
     * @param $transport
     * @return mixed
     * @throws \Exception
     */
    private function resolve($transport)
    {
        $types = $this->types();

        $transport = strtolower($transport);

        if(isset($types[$transport])) {
            return $types[$transport];
        }

        throw new \Exception('Invalid Transport: '. $transport);
    }

    /**
     * @return array
     */
    private function types() {

        return [
            'bus'    => Bus::class,
            'flight' => Flight::class,
            'train'  => Train::class
        ];
    }
}