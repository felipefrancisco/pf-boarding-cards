<?php

namespace App\Cards\Contracts;

/**
 * Interface Card
 * @package App\Cards\Contracts
 */
interface Card
{
    public function from();

    public function to();

    public function seat();

    public function toString();
}