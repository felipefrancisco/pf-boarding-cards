<?php

require_once dirname(__FILE__) . '/vendor/autoload.php';

$path = dirname(__FILE__) . '/resources/boarding_cards.json';

$cards = ( new \App\Cards\Sourcers\Sourcer($path) )->get();
$cardFactory = new \App\Cards\CardFactory();

foreach ($cards as &$card) {

    $card = $cardFactory->make( $card );
}

$tripCollection = new \App\Trip\TripCollection($cards);
$tripSorter = new \App\Trip\TripSorter($tripCollection);

$tripCollection = $tripSorter->sort();
echo $tripCollection->toString();