## Summary

This project was created using **PHP 7.0.13** and **Composer** to autoload PSR-4 compliant classes.

## Installation
Run [Composer](https://getcomposer.org/) to generate the PSR-4 autoloader and import `PHPUnit 6.3`

```
composer install
```

## Using the Application
Cards are sourced from `./resources/boarding_cards.json`

```
php run.php
```

## Tests Cases
Tests can be found on `./tests`.
```
./vendor/phpunit/phpunit/phpunit ./tests
```

---
Do not hesitate to contact me :) 

Felipe Francisco - felipefrancisco@outlook.com - +353 (0) 83 851 8599

